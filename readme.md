# Lab1

Run program with 3 parameters:
1. Type of action (E-encode, D-decode)
2. Path to encoded/decoded file.
3. Path to new file.

```sh
$ mvn exec:java -Dexec.mainClass=lab1.Lab1 -Dexec.args="E D:\workspace\ps\src\test\resources\img.bmp D:\workspace\ps\src\test\resources\e.b64"
$ mvn exec:java -Dexec.mainClass=lab1.Lab1 -Dexec.args="D D:\workspace\ps\src\test\resources\e.b64 D:\workspace\ps\src\test\resources\a.bmp"
```
