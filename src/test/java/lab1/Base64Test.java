package lab1;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Base64Test {

    @Test
    public void shouldConvertNoPaddingFileToString() throws IOException {
        byte[] bytes = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("noPadding.xml"));

        String expected = Base64.getEncoder().encodeToString(bytes);
        String actual = lab1.Base64.convertToString(bytes);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldConvertSinglePaddingFileToString() throws IOException {
        byte[] bytes = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("singlePadding.xml"));

        String expected = Base64.getEncoder().encodeToString(bytes);
        String actual = lab1.Base64.convertToString(bytes);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldConvertDoublePaddingFileToString() throws IOException {
        byte[] bytes = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("doublePadding.xml"));

        String expected = Base64.getEncoder().encodeToString(bytes);
        String actual = lab1.Base64.convertToString(bytes);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldConvertDocToString() throws IOException {
        byte[] bytes = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("a.doc"));

        String expected = Base64.getEncoder().encodeToString(bytes);
        String actual = lab1.Base64.convertToString(bytes);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldConvertZipToString() throws IOException {
        byte[] bytes = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("b.zip"));

        String expected = Base64.getEncoder().encodeToString(bytes);
        String actual = lab1.Base64.convertToString(bytes);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldConvertBmpToString() throws IOException {
        byte[] bytes = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("img.bmp"));

        String expected = Base64.getEncoder().encodeToString(bytes);
        String actual = lab1.Base64.convertToString(bytes);
        assertEquals(expected, actual);
    }

    @Test
    public void shouldConvertSinglePaddingToByteArray() throws IOException {
        byte[] expected = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("singlePadding.xml"));

        String str = Base64.getEncoder().encodeToString(expected);
        byte[] actual = lab1.Base64.convertToByteArray(str);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void shouldConvertDoublePaddingToByteArray() throws IOException {
        byte[] expected = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("doublePadding.xml"));

        String str = Base64.getEncoder().encodeToString(expected);
        byte[] actual = lab1.Base64.convertToByteArray(str);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void shouldConvertNoPaddingToByteArray() throws IOException {
        byte[] expected = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("noPadding.xml"));

        String str = Base64.getEncoder().encodeToString(expected);
        byte[] actual = lab1.Base64.convertToByteArray(str);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void shouldConvertDocToByteArray() throws IOException {
        byte[] expected = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("a.doc"));

        String str = Base64.getEncoder().encodeToString(expected);
        byte[] actual = lab1.Base64.convertToByteArray(str);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void shouldConvertZipToByteArray() throws IOException {
        byte[] expected = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("b.zip"));

        String str = Base64.getEncoder().encodeToString(expected);
        byte[] actual = lab1.Base64.convertToByteArray(str);
        assertArrayEquals(expected, actual);
    }

    @Test
    public void shouldConvertBmpToByteArray() throws IOException {
        byte[] expected = IOUtils.toByteArray(this.getClass().getClassLoader().getResourceAsStream("img.bmp"));

        String str = Base64.getEncoder().encodeToString(expected);
        byte[] actual = lab1.Base64.convertToByteArray(str);
        assertArrayEquals(expected, actual);
    }

}