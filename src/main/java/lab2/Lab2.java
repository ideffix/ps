package lab2;

import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyAdapter;
import lc.kra.system.keyboard.event.GlobalKeyEvent;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Lab2 {

    public static void main(String... args) {
        try {
            POP3ClientConfig config = new POP3ClientConfig("src/main/resources/App.config");
            POP3Client client = new POP3Client(config);
            ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
            executor.scheduleAtFixedRate(() -> {
                try {
                    client.connect();
                    client.login();
                    printInformationAboutUnreadMessages(client.getUnreadMessages());
                    client.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }, 0, config.getRefreshTime(), TimeUnit.SECONDS);
            GlobalKeyboardHook keyboardHook = new GlobalKeyboardHook(true);
            keyboardHook.addKeyListener(new GlobalKeyAdapter() {
                @Override
                public void keyPressed(GlobalKeyEvent event) {
                    if (event.getVirtualKeyCode()==GlobalKeyEvent.VK_Q) {
                        System.out.println(String.format("During app lifetime %d mails were read", client.getMessagesCount()));
                        executor.shutdown();
                        keyboardHook.shutdownHook();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printInformationAboutUnreadMessages(List<Message> unreadMessages) {
        System.out.println(String.format("You have %d unread messages.", unreadMessages.size()));
        unreadMessages.forEach(m -> System.out.println(String.format("Subject: %s\nFrom: %s\n", m.getSubject(), m.getFrom())));
    }

}
