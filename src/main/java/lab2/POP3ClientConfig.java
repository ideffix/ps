package lab2;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class POP3ClientConfig {
    private String serverAddress;
    private String username;
    private String password;
    private int port;
    private int refreshTime;

    public POP3ClientConfig(String filePath) throws IOException {
        loadProperties(filePath);
    }

    private void loadProperties(String filePath) throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(filePath));
        serverAddress = props.getProperty("serverAddress");
        username = props.getProperty("username");
        password = props.getProperty("password");
        port = Integer.valueOf(props.getProperty("port"));
        refreshTime = Integer.valueOf(props.getProperty("refreshTime"));
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getRefreshTime() {
        return refreshTime;
    }

    public void setRefreshTime(int refreshTime) {
        this.refreshTime = refreshTime;
    }
}
