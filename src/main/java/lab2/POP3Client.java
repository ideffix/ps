package lab2;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class POP3Client {

    private POP3ClientConfig config;
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private List<String> uniqueMessages = new ArrayList<>();
    private int messagesCount;

    public POP3Client(POP3ClientConfig config) {
        this.config = config;
    }

    public void connect() throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(config.getServerAddress(), config.getPort()));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        readResponse();
    }

    public void login() throws IOException {
        sendCommand(POP3Commands.USER_ID, config.getUsername());
        sendCommand(POP3Commands.PASSWORD, config.getPassword());
    }

    public void disconnect() throws IOException {
        sendCommand(POP3Commands.QUIT);
        socket.close();
        reader.close();
        writer.close();
    }

    public List<Message> getUnreadMessages() throws IOException {
        List<Message> messages = new ArrayList<>();
        String response = sendCommand(POP3Commands.NUMBER_OF_MESSAGES);
        int numberOfMessages = Integer.valueOf(response.split(" ")[1]);
        for (int i = 1; i <= numberOfMessages; i++) {
            fetchMessage(i).ifPresent(messages::add);
        }
        return messages;
    }

    private Optional<Message> fetchMessage(int i) throws IOException {
        String uidl = getUIDL(i);
        if (uniqueMessages.contains(uidl)) {
            return Optional.empty();
        }
        uniqueMessages.add(uidl);
        messagesCount++;
        String response, from, subject;
        from = subject = "";
        sendCommand(POP3Commands.FETCH_MESSAGE, String.valueOf(i));
        while ((response = readResponse()).length() != 0) {
            if (response.startsWith("From:")) {
                from = response.split(":")[1].trim();
            } else if (response.startsWith("Subject:")) {
                subject = response.split(":")[1].trim();
            }
        }
        StringBuilder body = new StringBuilder();
        while (!(response = readResponse()).equals(".")) {
            body.append(response).append("\n");
        }
        return Optional.of(new Message(subject, from, body.toString()));
    }

    private String getUIDL(int i) throws IOException {
        return sendCommand(POP3Commands.UIDL, String.valueOf(i)).split(" ")[2];
    }

    public String sendCommand(POP3Commands command) throws IOException {
        return sendCommand(command, "");
    }

    public String sendCommand(POP3Commands command, String additional) throws IOException {
        writer.write(command.getCommand() + " " + additional + "\n");
        writer.flush();
        return readResponse();
    }

    private String readResponse() throws IOException {
        String line = reader.readLine();
        if (line.startsWith("-ERR")) {
            throw new RuntimeException(line);
        }
        return line;
    }

    public int getMessagesCount() {
        return messagesCount;
    }
}
