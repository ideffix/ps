package lab2;

public class Message {
    private String subject;
    private String from;
    private String body;

    public Message(String subject, String from, String body) {
        this.subject = subject;
        this.from = from;
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
