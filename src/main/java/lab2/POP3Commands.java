package lab2;

public enum POP3Commands {
    USER_ID("user"),
    PASSWORD("pass"),
    MESSAGE_LIST("list"),
    NUMBER_OF_MESSAGES("stat"),
    FETCH_MESSAGE("retr"),
    DELETE_MESSAGE("dele"),
    UIDL("uidl"),
    QUIT("quit");

    private String command;

    POP3Commands(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

}
