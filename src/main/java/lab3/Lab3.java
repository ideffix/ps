package lab3;

import java.io.IOException;

public class Lab3 {

    public static void main(String... args) {
        try {
            SMTPConfig config = new SMTPConfig("src/main/resources/lab3.config");
            SMTPClient client = new SMTPClient(config);
            client.connect();
            client.sendEmail(new Message(config.getFrom(), config.getTo(), config.getSubject(), config.getBody()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
