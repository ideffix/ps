package lab3;

public enum SMTPCommands {
    INIT_CONNECTION("EHLO"),
    AUTH_LOGIN("AUTH LOGIN"),
    MAIL_FROM("MAIL FROM: "),
    MAIL_TO("RCPT TO: "),
    START_MESSAGE("DATA"),
    QUIT("QUIT");

    private String command;

    SMTPCommands(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}
