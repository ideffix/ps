package lab3;

import lab1.Base64;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

public class SMTPClient {
    private SMTPConfig config;
    private Socket socket;
    private BufferedWriter writer;

    public SMTPClient(SMTPConfig config) {
        this.config = config;
    }

    public void connect() throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(config.getServerAddress(), config.getPort()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    private void sendCommand(String command) throws IOException {
        writer.write(command + "\n");
        writer.flush();
    }

    private void sendCommand(SMTPCommands command) throws IOException {
        sendCommand(command.getCommand());
    }

    private void sendCommand(SMTPCommands command, String additional) throws IOException {
        sendCommand(command.getCommand() + additional);
    }

    public void sendEmail(Message message) throws IOException {
        sendCommand(SMTPCommands.INIT_CONNECTION);
        sendCommand(SMTPCommands.AUTH_LOGIN);
        sendCommand(Base64.convertToString(config.getUsername().getBytes()));
        sendCommand(Base64.convertToString(config.getPassword().getBytes()));
        sendCommand(SMTPCommands.MAIL_FROM, message.getFrom());
        sendCommand(SMTPCommands.MAIL_TO, message.getTo());
        sendCommand(SMTPCommands.START_MESSAGE);
        sendCommand(prepareMessage(message));
        sendCommand(SMTPCommands.QUIT);
    }

    private String prepareMessage(Message message) {
        StringBuilder sb = new StringBuilder();
        sb.append("From: " + message.getFrom() + "\n");
        sb.append("To:" + message.getTo() + "\n");
        sb.append("Subject: " + message.getSubject() + "\n\n");
        sb.append(message.getBody() + "\n.");
        return sb.toString();
    }
}
