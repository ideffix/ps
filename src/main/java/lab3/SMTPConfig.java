package lab3;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SMTPConfig {
    private String serverAddress;
    private String username;
    private String password;
    private String from;
    private String to;
    private String subject;
    private String body;
    private int port;

    public SMTPConfig(String filePath) throws IOException {
        loadProperties(filePath);
    }

    private void loadProperties(String filePath) throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(filePath));
        serverAddress = props.getProperty("serverAddress");
        username = props.getProperty("username");
        password = props.getProperty("password");
        from = props.getProperty("from");
        to = props.getProperty("to");
        from = props.getProperty("from");
        subject = props.getProperty("subject");
        body = props.getProperty("body");
        port = Integer.valueOf(props.getProperty("port"));
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
