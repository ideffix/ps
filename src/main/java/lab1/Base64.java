package lab1;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.primitives.Bytes;

import java.util.*;
import java.util.stream.Collectors;

public class Base64 {

    private static String BASE_64_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    public static String convertToString(byte[] bytes) {
        int padding = countPadding(bytes);
        byte[] splitted = split(addPadding(bytes, padding));
        return encode(splitted, padding);
    }

    private static int countPadding(byte[] bytes) {
        int padding = 0;
        while ((bytes.length + padding) % 3 != 0) {
            padding++;
        }
        return padding;
    }

    private static String encode(byte[] splitted, int padding) {
        StringBuilder sb = new StringBuilder();
        for (byte b : splitted) {
            sb.append(BASE_64_CHARS.charAt(b));
        }
        String sub = sb.toString().substring(0, sb.length() - padding);
        return sub + String.join("", Collections.nCopies(padding, "="));
    }

    private static byte[] addPadding(byte[] bytes, int padding) {
        byte[] padded = new byte[bytes.length + padding];
        System.arraycopy(bytes, 0, padded, 0, bytes.length);
        int diff = padding;
        while (diff-- > 0) {
            padded[bytes.length + diff] = 0;
        }
        return padded;
    }

    private static byte[] split(byte[] bytes) {
        List<byte[]> splitted = new ArrayList<>();

        for (int i = 0; i < bytes.length; i += 3) {
            byte[] group = new byte[3];
            for (int j = 0; j < 3; j++) {
                if (i + j < bytes.length) {
                    group[j] = bytes[i + j];
                }
            }
            splitted.add(group);
        }
        List<List<Byte>> result = new ArrayList<>();
        for (byte[] splittedBytes : splitted) {
            String reduce = Arrays.stream(convertToIntArray(splittedBytes))
                    .mapToObj(b -> Integer.toBinaryString(b & 0xFF))
                    .map(Base64::fillTo8)
                    .reduce("", String::concat);
            String[] byteParts = Iterables.toArray(Splitter.fixedLength(6)
                    .split(reduce), String.class);
            List<Byte> collect = Arrays.stream(byteParts)
                    .map(s -> (byte) Integer.parseInt("00" + s, 2))
                    .collect(Collectors.toList());
            result.add(collect);
        }
        return Bytes.toArray(result.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList()));
    }

    private static String fillTo8(String str) {
        while(str.length() < 8) {
            str = "0" + str;
        }
        return str;
    }

    private static String fillTo6(String str) {
        while(str.length() < 6) {
            str = "0" + str;
        }
        return str;
    }

    private static int[] convertToIntArray(byte[] input) {
        int[] ret = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            ret[i] = input[i];
        }
        return ret;
    }

    public static byte[] convertToByteArray(String str) {
        int paddingCount = countPadding(str);
        byte[] decoded = decode(str, paddingCount);
        return join(decoded, paddingCount);
    }

    private static int countPadding(String str) {
        return CharMatcher.is('=').countIn(str);
    }

    private static byte[] join(byte[] decoded, int paddingCount) {
        List<byte[]> groups = new ArrayList<>();

        for (int i = 0; i < decoded.length; i += 4) {
            byte[] group = new byte[4];
            for (int j = 0; j < 4; j++) {
                if (i + j < decoded.length) {
                    group[j] = decoded[i + j];
                }
            }
            groups.add(group);
        }

        List<List<Byte>> byteList = new ArrayList<>();
        for (byte[] group : groups) {
            String reduce = Arrays.stream(convertToIntArray(group))
                    .mapToObj(b -> Integer.toBinaryString(b & 0xFF))
                    .map(Base64::fillTo6)
                    .reduce("", String::concat);
            String[] byteParts = Iterables.toArray(Splitter.fixedLength(8)
                    .split(reduce), String.class);
            List<Byte> collect = Arrays.stream(byteParts)
                    .map(s -> (byte) Integer.parseInt(s, 2))
                    .collect(Collectors.toList());
            byteList.add(collect);
        }
        byte[] bytes = Bytes.toArray(byteList.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList()));
        byte[] res = new byte[bytes.length - paddingCount];
        System.arraycopy(bytes, 0, res, 0, bytes.length - paddingCount);
        return res;
    }

    private static byte[] decode(String str, int paddingCount) {

        byte[] decoded = new byte[str.length() - paddingCount];
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length - paddingCount; i++) {
            decoded[i] = (byte) BASE_64_CHARS.indexOf(chars[i]);
        }
        return decoded;
    }

}
