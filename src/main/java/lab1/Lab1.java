package lab1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class Lab1 {

    private static final String ENCODE = "E";
    private static final String DECODE = "D";

    public static void main(String... args) {
        if (validate(args)) {
            try {
                Path path = loadFile(args[1]);
                if (ENCODE.equals(args[0])) {
                    encodeToFile(path, args[2]);
                } else {
                    decodeToFile(path, args[2]);
                }
            } catch (IOException e) {
                System.err.println("Problem with file: " + args[1]);
            }
        }
    }

    private static void decodeToFile(Path path, String filePath) throws IOException {
        byte[] bytes = Base64.convertToByteArray(Files.readAllLines(path).stream()
                .collect(Collectors.joining()));
        Files.write(Paths.get(filePath), bytes);
    }

    private static void encodeToFile(Path path, String filePath) throws IOException {
        String encoded = Base64.convertToString(Files.readAllBytes(path));
        Files.write(Paths.get(filePath), encoded.getBytes());
    }

    private static Path loadFile(String path) throws FileNotFoundException {
        Path p = Paths.get(path);
        if (Files.exists(p)) {
            return p;
        } else {
            throw new FileNotFoundException();
        }
    }

    private static boolean validate(String[] args) {
        if (args == null) {
            System.err.println("Args cannot be null");
            return false;
        }
        if (args.length != 3) {
            System.err.println("Invalid parameter count");
            return false;
        }
        if (!args[0].equals(ENCODE) && !args[0].equals(DECODE)) {
            System.err.println("Invalid action type. Allowed are: E(encode) or D(decode)");
            return false;
        }
        return true;
    }
}
