package lab5;

import java.util.ArrayList;
import java.util.List;

public class File {
    private int depth;
    private String site;
    private List<String> images = new ArrayList<>();
    private List<String> emails = new ArrayList<>();
    private List<File> files = new ArrayList<>();

    public File(String site, int depth) {
        this.site = site;
        this.depth = depth;
    }

    public void addImage(String image) {
        System.out.println("Adding new image: " + image);
        images.add(image);
    }

    public void addEmail(String email) {
        System.out.println("Adding new email: " + email);
        emails.add(email);
    }

    public void addFile(File file) {
        System.out.println("Adding new file: " + file.getSite());
        files.add(file);
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
}
