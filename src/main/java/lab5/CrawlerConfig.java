package lab5;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class CrawlerConfig {
    private String site;
    private int depth;

    public CrawlerConfig(String filePath) throws IOException {
        loadProperties(filePath);
    }

    private void loadProperties(String filePath) throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(filePath));
        site = props.getProperty("site");
        depth = Integer.valueOf(props.getProperty("depth"));
    }

    public String getSite() {
        return site;
    }

    public int getDepth() {
        return depth;
    }
}
