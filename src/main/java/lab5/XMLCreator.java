package lab5;

import java.util.Collections;

import static java.lang.String.format;

public class XMLCreator {

    public static String create(File rootFile, int depth) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
        sb.append(fromSingleFile(rootFile, true, depth));
        return sb.toString();
    }

    private static String fromSingleFile(File file, boolean isRootFile, int depth) {
        StringBuilder sb = new StringBuilder();
        sb.append(format(getIndent(file.getDepth() - 1) + "<%s url=\"%s\"%s>\n", isRootFile ? "SITE" : "FILE", file.getSite(), isRootFile ? addDepth(depth) : ""));
        file.getImages().forEach(img -> sb.append(format(getIndent(file.getDepth()) + "<IMAGE>%s</IMAGE>\n", img)));
        file.getEmails().forEach(email -> sb.append(format(getIndent(file.getDepth()) + "<EMAIL>%s</EMAIL>\n", email)));
        file.getFiles().forEach(f -> sb.append(fromSingleFile(f, false, depth)));
        sb.append(getIndent(file.getDepth() - 1) + format("</%s>\n", isRootFile ? "SITE" : "FILE"));
        return sb.toString();
    }

    private static String addDepth(int depth) {
        return format(" depth=\"%d\"", depth);
    }

    private static String getIndent(int count) {
        return String.join("", Collections.nCopies(count, "\t"));
    }
}
