package lab5;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Crawler {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private String site;
    private int depth;
    private File rootFile;
    private List<String> visitedSites = new ArrayList<>();

    public Crawler(CrawlerConfig crawlerConfig) {
        this.site = crawlerConfig.getSite();
        this.depth = crawlerConfig.getDepth();
        this.rootFile = new File(site, 1);
    }

    public String crawl() throws IOException {
        crawlForFile(rootFile);
        return XMLCreator.create(rootFile, depth);
    }

    private void crawlForFile(File file) throws IOException {
        if (visitedSites.contains(file.getSite())) {
            return;
        }
        visitedSites.add(file.getSite());
        HttpHandler httpHandler = new HttpHandler(file.getSite());
        List<String> siteContent = httpHandler.getSiteContent();
        Matcher matcher;
        for (String line : siteContent) {
            line = line.trim();
            if (line.contains("<img ") || line.contains("</img>")) {
                getValues(line, "src").forEach(src -> {
                    if (containsProtocol(src)) {
                        file.addImage(src);
                    } else {
                        file.addImage(resolveUrl(httpHandler, src));
                    }
                });
            } else if (line.contains("<a ") || line.contains("</a>")) {
                getValues(line, "href").forEach(href -> {
                    if (href.endsWith(".html") || href.endsWith(".htm")) {
                        if (containsProtocol(href)) {
                            file.addFile(new File(href, file.getDepth() + 1));
                        } else {
                            file.addFile(new File(resolveUrl(httpHandler, href), file.getDepth() + 1));
                        }
                    } else if(href.contains("mailto")) {
                        file.addEmail(href.split(":")[1]);
                    }
                });

            } else if ((matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(line)).find()) {
                file.addEmail(matcher.group());
            }
        }
        if (file.getDepth() < depth) {
            for (File subfile : file.getFiles()) {
                crawlForFile(subfile);
            }
        }
    }

    private String resolveUrl(HttpHandler httpHandler, String src) {
        String[] split = httpHandler.getSubsite().split("/");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < split.length - 1; i++) {
            if (!split[i].isEmpty()) {
                sb.append("/" + split[i]);
            }
        }
        sb.append("/" + src);
        return httpHandler.getProtocol() + "://" + (httpHandler.getSite() + "/"  + sb.toString()).replace("//", "/");
    }

    private boolean containsProtocol(String url) {
        return url.contains("https://") || url.contains("http://") || url.contains("www.");
    }

    private Stream<String> getValues(String line, String key) {
        Stream<String> stream = Stream.empty();
        Optional<String> opt;
        while((opt = getValue(line, key)).isPresent()) {
            stream = Stream.concat(stream, Stream.of(opt.get()));
            line = line.replaceFirst(key, "");
        }
        return stream;
    }

    private Optional<String> getValue(String line, String key) {
        line = line.replace("= ", "=");
        if (line.indexOf(key + "=\"") >= 0) {
            return Optional.of(line.substring(line.indexOf(key + "=\"")).split("\"")[1]);
        } else if (line.indexOf(key + "=") >= 0) {
            String withoutQuotes = line.split("=")[1].split(" ")[0].replace(">", "");
            return withoutQuotes.contains("#") ? Optional.empty() : Optional.of(withoutQuotes);
        }
        return Optional.empty();
    }

}
