package lab5;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class HttpHandler {
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private String site;
    private String subsite;
    private String protocol;


    public HttpHandler(String site) throws MalformedURLException {
        resolveSiteAndSubsite(site);
    }

    private void resolveSiteAndSubsite(String site) throws MalformedURLException {
        URL url = new URL(site);
        this.site = url.getHost();
        this.subsite = url.getPath().isEmpty() ? "/" : url.getPath();
        this.protocol = url.getProtocol();
    }

    public List<String> getSiteContent() throws IOException {
        connect();
        List<String> siteContent = getLinesList();
        disconnect();
        return siteContent;
    }

    private void connect() throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(site, 80));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    private void disconnect() throws IOException {
        socket.close();
        reader.close();
        writer.close();
    }

    private void sendGetRequest() throws IOException {
        writer.write(format("GET %s HTTP/1.1\r\n", subsite));
        writer.write(format("Host: %s\r\n\r\n", site));
        writer.flush();
    }

    private List<String> getLinesList() throws IOException {
        sendGetRequest();
        return readResponse();
    }

    private List<String> readResponse() {
        return reader.lines().collect(Collectors.toList());
    }

    public String getSite() {
        return site;
    }

    public String getSubsite() {
        return subsite;
    }

    public String getProtocol() {
        return protocol;
    }
}
