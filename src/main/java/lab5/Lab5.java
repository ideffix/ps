package lab5;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class Lab5 {

    public static void main(String... args) throws IOException {
        CrawlerConfig config = new CrawlerConfig("src/main/resources/lab5.config");
        Crawler crawler = new Crawler(config);
        String xml = crawler.crawl();
        saveToFile(xml, config.getSite());
    }

    private static void saveToFile(String xml, String site) throws FileNotFoundException {
        String filename = site.replace("https://", "").replace("http://", "") + ".xml";
        PrintWriter out = new PrintWriter(filename);
        out.println(xml);
        out.close();
        System.out.println("Crawler result saved to file: " + filename);
    }

}
